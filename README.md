## Nation Feathers VR
![Alt text](/NFVR_Sample.png)
Nation Feathers VR is a virtual reality learning game developed in Unity for the Samsung Gear VR as part of an internship project at
Cornell Lab of Ornithology's Bioacoustics Research Program. The game aims at teaching players to identify bird species by their 
vocalizations using visual, textual and auditory means.

## Concept
The game takes place in a forest-like environment where players encounter different bird songs and calls. Using Unity's 3D sound technology, 
players attempt to locate the vocalizing birds and upon finding them are shown an image and the name of the respective bird.
This plot is expanded upon in the second iteration of the game where players are not only tasked with finding the birds, but are also subsequently
given a chance to test their knowledge by identifying the birds themselves.

---

## Hardware Requirements and Installation
The game was tested solely on a Samsung Note 5 and is thus guaranteed to run smoothly on that device. There should, however, be no reason why the game would not run on any of the devices Samsung considers compatible with the Gear VR. These are:
	
* Galaxy S8/S8+
+ Galaxy S7/S7 edge
- Galaxy S6/S6 edge/S6 edge+
	
Needless to say, a Samsung Gear VR is also needed to run the game. For testing, the 2017 version was used, but it is also safe to assume older versions would work as well. Note, however, that older versions did not come with a controller and one would have to be purchased separately.
It should be emphasized that the game has only been tested and proven to run on the devices mentioned above and are only guaranteed to run on them.
To install the game on the target device:
	
1. Go to **Settings** > **Security** and activate **Unknown sources**
2. Copy NFVR.apk file to Android device.
3. Once copied, the .apk file can be found on the device using a file manager application.
4. Install by tapping on file.
---	
In-depth information on the project including implementation details can be found in **Final_Report.pdf**.